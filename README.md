# 10. CKA Pratice Exam Part 10 Acg : Déterminer et résoudre les problèmes des nœuds disfonctionnels 

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectif

1. Déterminer ce qui ne va pas avec le nœud en panne

    a. Il y a un problème avec l'un des nœuds du cluster. Explorez le cluster et déterminez quel nœud est cassé, puis enregistrez le nom de ce nœud dans le fichier` /k8s/0004/broken-node.txt`.

    b. Explorez le nœud cassé et déterminez ce qui ne va pas.

2. Résoudre le problème

    Résolvez le problème avec le nœud cassé afin qu'il redevienne READY.

# Context

Cet atelier propose des scénarios pratiques pour vous aider à vous préparer à l'examen Certified Kubernetes Administrator (CKA). Des tâches à accomplir vous seront présentées, ainsi que des serveurs et/ou un cluster Kubernetes existant pour les accomplir. Vous devrez utiliser vos connaissances de Kubernetes pour mener à bien les tâches fournies, un peu comme vous le feriez sur le véritable examen CKA. Bonne chance!

Cette question utilise le acgk8scluster. Après vous être connecté au serveur d'examen, passez au contexte correct avec la commande kubectl config use-context acgk8s.

Chacun des objectifs représente une tâche que vous devrez accomplir en utilisant le cluster et les serveurs disponibles. Lisez attentivement chaque objectif et accomplissez la tâche spécifiée.

Pour certains objectifs, vous devrez peut-être vous connecter à d'autres nœuds ou serveurs à partir du serveur d'examen. Vous pouvez le faire en utilisant le nom d'hôte/nom de nœud, c'est-à-dire ssh acgk8s-worker1.

Remarque : Vous ne pouvez pas vous connecter à un autre nœud, ni l'utiliser kubectlpour vous connecter au cluster, à partir d'un nœud autre que le nœud racine. Une fois que vous avez terminé les tâches nécessaires sur un serveur, assurez-vous de exitrevenir au nœud racine avant de continuer.

Si vous devez assumer les privilèges root sur un serveur, vous pouvez le faire avec sudo -i.

Vous pouvez exécuter le script de vérification situé à /home/cloud_user/verify.shtout moment pour vérifier votre travail !


>![Alt text](img/image.png)

# Application

#### Étape 1 : Connexion au Serveur

1. Connectez-vous au serveur à l'aide des informations d'identification fournies :

```sh
ssh cloud_user@<PUBLIC_IP_ADDRESS>
```

#### Étape 2 : Déterminer ce qui ne va pas avec le Nœud Cassé

1. Passez au contexte `acgk8s` :

```sh
kubectl config use-context acgk8s
```

2. Vérifiez l'état actuel des nœuds :

```sh
kubectl get nodes
```

3. Identifiez le nœud cassé et enregistrez le nom dans un fichier nommé `broken-node.txt` :

```sh
echo acgk8s-worker2 > /k8s/0004/broken-node.txt
```

>![Alt text](img/image-1.png)

4. Essayez de déterminer la cause du problème :

```sh
kubectl describe node acgk8s-worker2
```

>![Alt text](img/image-2.png)

#### Étape 3 : Résoudre le Problème

1. Accédez au nœud en utilisant `ssh` :

```sh
ssh acgk8s-worker2
```

2. Vérifiez le journal de `kubelet` :

```sh
sudo journalctl -u kubelet
```

>![Alt text](img/image-3.png)

3. Vérifiez la dernière entrée du journal.

>![Alt text](img/image-4.png)

4. Vérifiez l'état de `kubelet` :

```sh
sudo systemctl status kubelet
```
>![Alt text](img/image-5.png)

5. Activez `kubelet` :

```sh
sudo systemctl enable kubelet
```

6. Démarrez `kubelet` :

```sh
sudo systemctl start kubelet
```

7. Vérifiez que le démarrage de `kubelet` a réussi :

```sh
sudo systemctl status kubelet
```

>![Alt text](img/image-6.png)

8. Revenez au nœud du plan de contrôle :

```sh
exit
```

9. Vérifiez l'état des nœuds :

```sh
kubectl get nodes
```

>![Alt text](img/image-7.png)

10. Vérifiez l'état des objectifs de l'examen :

```sh
./verify.sh
```

En suivant ces étapes, vous aurez identifié et résolu les problèmes du nœud cassé dans votre cluster Kubernetes.